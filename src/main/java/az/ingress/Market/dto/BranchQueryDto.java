package az.ingress.Market.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BranchQueryDto {
    Long branchId;
    String branchName;
    String addressName;
    String marketName;
}
