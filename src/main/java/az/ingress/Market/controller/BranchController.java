package az.ingress.Market.controller;

import az.ingress.Market.model.Branch;
import az.ingress.Market.repository.BranchRepository;
import az.ingress.Market.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/branch")
@RequiredArgsConstructor
public class BranchController {
    private final BranchService branchService;
    private final BranchRepository branchRepository;

    @PostMapping
    public Branch createBranch(@RequestBody Branch branch) {
        Branch savedBranch = branchService.saveBranchCascadeAll(branch);
        return branch;
    }

    @GetMapping("/{id}")
    public Branch getBranch(@PathVariable Long id) {
       return Optional.ofNullable
               (branchRepository.findBranchWithMarketAndAddressById(id))
               .orElseThrow(()-> new RuntimeException("Not found id"+ " " + id));
    }


    @DeleteMapping("/{id}")
    public Long deleteBranch(@PathVariable Long id){
       return branchService.deleteBranch(id);
    }




//    @GetMapping
//    public Branch getBranch() {
//        return branchService.getBranch();
//    }

}
