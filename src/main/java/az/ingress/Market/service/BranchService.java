package az.ingress.Market.service;

import az.ingress.Market.dto.RequestBranchDto;
import az.ingress.Market.model.Branch;

import java.util.Optional;

public interface BranchService {
    long saveBranch(RequestBranchDto request);

    Branch saveBranchCascadeAll(Branch branch);

    Long deleteBranch(Long id);



    //    Branch getBranch();
}
