package az.ingress.Market.service;

import az.ingress.Market.dto.RequestAddressDto;
import az.ingress.Market.model.Address;

public interface AddressService {
    long saveAddress(RequestAddressDto request);
    Address findById(long id);
}
