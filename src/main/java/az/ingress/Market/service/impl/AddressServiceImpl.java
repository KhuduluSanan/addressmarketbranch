package az.ingress.Market.service.impl;

import az.ingress.Market.dto.RequestAddressDto;
import az.ingress.Market.model.Address;
import az.ingress.Market.repository.AddressRepository;
import az.ingress.Market.service.AddressService;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;
    private final ModelMapper modelMapper;
    @Override
    public long saveAddress(RequestAddressDto request) {
        Address address = modelMapper.map(request, Address.class);
        return addressRepository.save(address).getId();
    }

    @Override
    public Address findById(long id) {
        Optional<Address> addressOptional = addressRepository.findById(id);
        addressOptional.orElseThrow(() -> new RuntimeException());
        return addressOptional.get();
    }
}
