package az.ingress.Market.service.impl;

import az.ingress.Market.dto.RequestBranchDto;
import az.ingress.Market.model.Address;
import az.ingress.Market.model.Branch;
import az.ingress.Market.model.Market;
import az.ingress.Market.repository.AddressRepository;
import az.ingress.Market.repository.BranchRepository;
import az.ingress.Market.repository.MarketRepository;
import az.ingress.Market.service.AddressService;
import az.ingress.Market.service.BranchService;
import az.ingress.Market.service.MarketService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class BranchServiceImpl implements BranchService {

    private final BranchRepository branchRepository;
    private final MarketRepository marketRepository;
    private final AddressRepository addressRepository;
    private final MarketService marketService;
    private final AddressService addressService;

    @Override
    public long saveBranch(RequestBranchDto request) {
        Address address = addressService.findById(request.getAddress_id());
        Market market = marketService.getMarketWithId(request.getAddress_id());
        Branch branch = Branch.builder()
                .branchName(request.getBranchName())
                .address(address)
                .market(market)
                .build();
        return branchRepository.save(branch).getId();
    }


    @Override
    public Branch saveBranchCascadeAll(Branch branch) {
        Market market = branch.getMarket();
        Address address = branch.getAddress();
        address = addressRepository.save(address);
        market = marketRepository.save(market);
        branch.setAddress(address);
        branch.setMarket(market);
        return branchRepository.save(branch);
    }

    @Override
    public Long deleteBranch(Long id) {
        Optional<Branch> branchOptional = branchRepository.findById(id);
        if (branchOptional.isPresent()) {
            branchRepository.deleteById(id);
        } else {
            throw new RuntimeException("Not deleted this ID but not found - " + " " +id);
        }
        return  id  ;
    }


//    @Override
//    public Branch getBranch() {
//        Branch branch = branchRepository.getBranchJpql();
//        log.info("This is {}" , branch);
//        return branch;
//    }
}
