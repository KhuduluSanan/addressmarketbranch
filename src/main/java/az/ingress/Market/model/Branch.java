package az.ingress.Market.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Branch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String branchName;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "m_id", referencedColumnName = "id")
    Market market;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "a_id", referencedColumnName = "id")
    Address address;
}
